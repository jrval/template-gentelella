<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;
//use Spatie\Permission\Models\Permission;
use Yajra\DataTables\Facades\DataTables;
use App\Permission;


class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('permission_view')) {
            return abort(403);
        }
        return view('app.pages.permissions.index');
    }

    public function permissionDatatable(){
        if (! Gate::allows('permission_view')) {
            return abort(403);
        }
        $data = Permission::query();
        return DataTables::of($data)
            ->addColumn('action', function ($data) {
                $html='<a href="" type="button" data-btn="edit" data-id="'.$data->id.'" data-toggle="modal" data-target="#modal-lg">
                        <span class="label label-info">Edit</span>
                        </a>';
                $html.='<a href="" type="button" data-btn="delete" data-id="'.$data->id.'" data-toggle="modal" data-target="#modal-lg-delete">
                        <span class="label label-danger">Delete</span>
                        </a>';
                return  $html;
            })
            ->rawColumns(['action'])
            ->toArray();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('permission_create')) {
            return abort(403);
        }
        //render form
        $form = view()->make('app.pages.permissions._form')->render();
        //save to variable
        $data = array(
            'form' => $form,
        );
        //return json
        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! Gate::allows('permission_create')) {
            return abort(403);
        }
        $request->validate([
            'name' => 'required|unique:permissions,name|regex:/(^[A-Za-z0-9-_]+$)+/',
        ]);
        $permission = Permission::create($request->all());
        if($permission){
            return  response()->json([
                'status'=>'success',
                'message'=>'Permission Successfully Added'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('permission_update')) {
            return abort(403);
        }
        $permission = Permission::find($id);
        $form = view()->make('app.pages.permissions._form',compact('permission'))->render();
        //save to variable
        $data = array(
            'form' => $form,
        );
        //return json
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! Gate::allows('permission_update')) {
            return abort(403);
        }
        $request->validate([
            'name' => 'required|unique:permissions,name,'.$request->permission.'|regex:/(^[A-Za-z0-9-_]+$)+/',
        ]);
        $permission = Permission::find($id);
        $permission->name = $request->name;
        $permission->save();

        if($permission){
            return  response()->json([
                'status'=>'success',
                'message'=>'Permission Successfully Updated',
                'name'=>$request->name
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('permission_delete')) {
            return abort(403);
        }
        $permission = \Spatie\Permission\Models\Permission::findOrFail($id);
        $permission->delete();

        if($permission){
            return  response()->json([
                'status'=>'success',
                'message'=>'Permission Successfully Deleted',
            ]);
        }
    }


}
