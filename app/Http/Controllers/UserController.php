<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('user_view')) {
            return abort(403);
        }
        return view('app.pages.users.index');
    }

    public function usersDatatable(){
        if (! Gate::allows('user_view')) {
            return abort(403);
        }
        $data = User::usersDatatable();
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()

    {
        if (! Gate::allows('user_create')) {
            return abort(403);
        }
        $roles = Role::all();
        //render form
        $form = view()->make('app.pages.users._form',compact('roles'))->render();
        //save to variable
        $data = array(
            'form' => $form,
        );
        //return json
        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        if (! Gate::allows('user_create')) {
            return abort(403);
        }
        $user = User::userStore($request);
        return $user;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('user_view')) {
            return abort(403);
        }
        $user = User::findorfail($id);
        return view('app.pages.users.show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('user_update')) {
            return abort(403);
        }
        $user = User::find($id);
        $roles = Role::all();
        $form = view()->make('app.pages.users._form',compact('user','roles'))->render();
        //save to variable
        $data = array(
            'form' => $form,
        );
        //return json
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        if (! Gate::allows('user_update')) {
            return abort(403);
        }
        $user = User::findOrFail($id);
        $user->update($request->except('roles','password_confirmation'));
        $roles = $request->input('roles') ? $request->input('roles') : [];
        $user->syncRoles($roles);
        if($user){
            return  response()->json([
                'status'=>'success',
                'message'=>'User Successfully Updated'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    { //find user
        if (! Gate::allows('user_disable')) {
            return abort(403);
        }
        $user = User::findOrFail($id);
        //delete user
        $user->delete();

        if($user){
            return  response()->json([
                'status'=>'success',
                'message'=>'User Successfully disabled',
            ]);
        }
    }

    public function userEnabled($id)
    { //find user
        if (! Gate::allows('user_enable')) {
            return abort(403);
        }
        $user = User::withTrashed()->find($id)->restore();
        if($user){
            return  response()->json([
                'status'=>'success',
                'message'=>'User Successfully enabled',
            ]);
        }
    }
}
