<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
                $rules = [
                    'name' => 'required',
                    'username' => 'sometimes|max:255|unique:users,username,'.$this->user.'|regex:/(^[A-Za-z0-9-_]+$)+/',
                    'email' => 'required|email|max:255|unique:users,email,'.$this->user,
                    'password' => 'sometimes',
                    'password_confirmation' => 'sometimes|same:password',
                    'roles' => 'required',
                ];
                break;

            default:
                $rules = [
                    'name' => 'required',
                    'username' => 'sometimes|max:255|unique:users,username|regex:/(^[A-Za-z0-9-_]+$)+/',
                    'email' => 'required|email|max:255|unique:users,email',
                    'password' => 'required',
                    'password_confirmation' => 'required|same:password',
                    'roles' => 'required',
                ];
                break;

        }
        return $rules;

    }

//    public function attributes()
//    {
//        return [
//            'username' => 'username',
//            'password' => 'password',
//            'name' => 'name',
//            'roles' => 'roles',
//        ];
//    }
}
