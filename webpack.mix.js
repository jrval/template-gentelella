const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
//
// mix.js('resources/js/app.js', 'public/js')
//    .sass('resources/sass/app.scss', 'public/css');

// mix.styles(['resources/theme/vendors/bootstrap/dist/css/bootstrap.min.css',
//     'resources/theme/vendors/font-awesome/css/font-awesome.min.css',
//     'resources/theme/vendors/nprogress/nprogress.css',
//     'resources/theme/vendors/animate.css/animate.min.css',
//     'resources/theme/vendors/iCheck/skins/flat/green.css'
// ],'public/theme/vendors/vendors.css');

mix.copy(['resources/theme/vendors'
],'public/theme/vendors');
mix.copy(['resources/theme/build/images'
],'public/theme/images');

mix.styles(['resources/theme/build/css/custom.css'],'public/theme/css/custom.css');

mix.scripts(['resources/theme/build/js/custom.js'],'public/theme/js/custom.js');
