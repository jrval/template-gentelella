<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <h3>Dashboard</h3>
        <ul class="nav side-menu">
            <li class="{{ Request::segment(1) == 'home'  ? 'active' : ''}} {{Request::segment(1) == ''  ? 'active' : '' }}"><a href="{{route('home')}}"><i class="fa fa-home"></i> Home</a></li>
        </ul>
    </div>
    <div class="menu_section">
        <h3>Administrator</h3>
        <ul class="nav side-menu">
            <li><a><i class="fa fa-users"></i> User Management <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li class="{{ Request::segment(1) == 'permissions'  ? 'active' : ''}}"><a href="{{route('permissions.index')}}">Permissions</a></li>
                    <li class="{{ Request::segment(1) == 'roles'  ? 'active' : ''}}"><a href="{{route('roles.index')}}">Roles</a></li>
                    <li class="{{Request::segment(1) == 'users'  ? 'active' : ''}}"><a href="{{route('users.index')}}">Users</a></li>
                </ul>
            </li>
      </ul>
    </div>
</div>
