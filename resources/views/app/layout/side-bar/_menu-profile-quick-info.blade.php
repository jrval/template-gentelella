<div class="profile clearfix">
    <div class="profile_pic">
        <img src="{{asset('theme')}}/images/img.jpg" alt="..." class="img-circle profile_img">
    </div>
    <div class="profile_info">
        <span>Welcome,</span>
        <h2>{{auth()->user()->name}}</h2>
    </div>
</div>
