<div class="modal fade bs-example-modal-lg1" tabindex="-1" role="dialog" aria-hidden="true" id="modal-lg">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <div class="alert alert-danger  error-modal" role="alert" style="display: none;">
                </div>
                <div class="alert alert-success  success-modal" role="alert" style="display: none;">
                </div>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-modal-submit"></button>
            </div>

        </div>
    </div>
</div>
