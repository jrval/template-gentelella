<div class="modal fade bs-example-modal-lg1" tabindex="-1" role="dialog" aria-hidden="true" id="modal-lg-delete">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <div class="alert alert-danger error-modal"  role="alert" style="display: none;">
                </div>
                <div class="alert alert-success success-modal" role="alert" style="display: none;">
                </div>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body">
                <div id="message"></div>
                <form id="form-delete" method="POST" action="">
                    <input type="hidden" name="_method" value="DELETE">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger btn-modal-submit">Delete</button>
            </div>

        </div>
    </div>
</div>
