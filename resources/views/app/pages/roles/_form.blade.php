<form id="form" class="form-horizontal form-label-left">
    @csrf
    @isset($role)
        {{ method_field('PUT') }}
    @endisset
    <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Role Name <span class="required">*</span>
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" name="name" required="required" value="{{$role->name ?? ''}}" class="form-control col-md-7 col-xs-12">
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 col-sm-3 col-xs-12 control-label">Select Permission
            <br>
            <small class="text-navy">Role can have 1 or more permissions</small>
        </label>
        <div class="col-md-9 col-sm-9 col-xs-12">

                @forelse($permissions as $key=>$permission)
                <div class="checkbox">
                    <label>
                        @if(isset($role))
                            <input type="checkbox" class="flat" name="permissions[]" value="{{$permission->name}}"
                                   @foreach($role->permissions()->pluck('name') as $roles)
                                           @if($roles== $permission->name)
                                           checked="checked"
                                        @endif
                                @endforeach
                            > {{$permission->name}}
                        @else
                            <input type="checkbox" class="flat" name="permissions[]" value="{{$permission->name}}" > {{$permission->name}}
                        @endif
                    </label>
                </div>
                @empty
                <div class="checkbox">
                    <label>
                       No Permissions
                    </label>
                </div>
                @endforelse
        </div>
    </div>
</form>
