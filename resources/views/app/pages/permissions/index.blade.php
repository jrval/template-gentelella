@extends("app.layout.app")
@section("title","Users")
@push("page-styles")
    <!-- bootstrap-progressbar -->
    <link href="{{asset("theme/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css")}}" rel="stylesheet">

    <!-- Datatables -->
    <link href="{{asset('theme')}}/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="{{asset('theme')}}/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="{{asset('theme')}}/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="{{asset('theme')}}/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="{{asset('theme')}}/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
@endpush

@section("content")
    <div style="margin-top: 50px;">
        <div class="page-title">
            <div class="title_left">
                <h3>Permissions</h3>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <button type="button" class="btn btn-success" data-btn="create" data-toggle="modal" data-target="#modal-lg"><i class="fa fa-plus"></i> Create Permission</button>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="container">
                            <table id="permissions-datatable" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Created At</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push("page-scripts")
    <!-- Datatables -->


    <script src="{{asset('theme')}}/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('theme')}}/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="{{asset('theme')}}/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="{{asset('theme')}}/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="{{asset('theme')}}/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="{{asset('theme')}}/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="{{asset('theme')}}/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="{{asset('theme')}}/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="{{asset('theme')}}/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="{{asset('theme')}}/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="{{asset('theme')}}/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="{{asset('theme')}}/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="{{asset('theme')}}/vendors/jszip/dist/jszip.min.js"></script>
    <script src="{{asset('theme')}}/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="{{asset('theme')}}/vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="{{asset('theme')}}/vendors/validator/validator.js"></script>
    <script src="{{asset('theme')}}/vendors/moment/moment.js"></script>
@endpush

@push("scripts")
    @include('app.pages.permissions._scripts')
@endpush
