<script>
    var btn;
    var modal;
    var form;
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#permissions-datatable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,

            ajax:{
                "url": '{!! route("permissions.datatable") !!}',
                "type": "GET",
            },
            columns: [
                { data: 'name', name: 'name' },
                { data: 'created_at', name: 'created_at'},
                { data: 'action', name: 'action',searchable:false,orderable:false},

            ],
            columnDefs:[
                {targets:2,"width":"10%"}
            ],
            "order": [[ 0, "asc" ]], //or asc
        });

    });

    /*render create-edit modal*/
    $('#modal-lg').on('show.bs.modal', function (event) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var button = $(event.relatedTarget); // Button that triggered the modal
        btn = button.data('btn'); // Extract info from data-* attributes
        modal = $(this);
        form = modal.find(".modal-body #form");
        form.trigger('reset');
        if (btn === 'create') {
            var url = '{{ route("permissions.create")}}';
            $.ajax({
                url: url, //this is your uri
                type: 'get', //this is your method
                dataType: 'json',
                success: function (response) {
                    //console.log(response.form);
                    modal.find('.modal-title').html('CREATE PERMISSION');
                    modal.find('.modal-body').html(response.form);
                    form = modal.find(".modal-body #form");
                    form.attr('action', '{{ route("permissions.store")}}');
                    form.attr('method', 'POST');
                    modal.find('.btn-modal-submit').html('Save');
                }
            })//end ajax
        } else if (btn === 'edit') {
            var permission_id = button.data('id');
            var url = '{{ route("permissions.edit",":permission_id")}}';
            url = url.replace(':permission_id', permission_id);
            $.ajax({
                url: url, //this is your uri
                type: 'get', //this is your method
                dataType: 'json',
                success: function (response) {
                    //console.log(response.form);
                    var url = '{{ route("permissions.update",":permission_id")}}';
                    url = url.replace(':permission_id', permission_id);
                    modal.find('.modal-title').html('EDIT PERMISSION');
                    modal.find('.modal-body').html(response.form);
                    form = modal.find(".modal-body #form");
                    form.attr('action', url);
                    form.attr('method', 'POST');
                    modal.find('.btn-modal-submit').html('Update');
                }
            })//end ajax
        }
    });

    $('#modal-lg-delete').on('show.bs.modal', function (event) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var button = $(event.relatedTarget); // Button that triggered the modal
        var permission_id = button.data('id');// Extract info from data-* attributes
        var url = '{{ route("permissions.destroy", ":permission_id") }}';
        url = url.replace(':permission_id', permission_id);
        modal = $(this);
        form = modal.find('.modal-body #form-delete');
        console.log(form);
        form.attr('action', url);
        console.log(form.attr('action', url));
        modal.find('.modal-title').html('DELETE PERMISSION');
        modal.find('.modal-body #message').html(' <strong>Permission will be DELETED!</strong> Are you sure to process this transaction?');
    });
    /*AJAX SUBMIT*/
    $(".btn-modal-submit").click(function (e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        var url =  form.attr('action');
        console.log(url);
        $.ajax({
            type: form.attr('method'),
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function (data) {
                checkValidation(form,data);
                $('#permissions-datatable').DataTable().ajax.reload();
                if(btn !== 'delete'){
                    modal.find('.modal-body #name').val(data.name)
                }
            }, error: function (xhr, ajaxOptions, thrownError) {
                checkValidation(form,xhr)
            }
        });
    });

    /*functions*/

    function checkValidation(form,data){
        if(data.status==='success'){
            var success = modal.find('.success-modal');
            success.fadeIn();
            success.html(data.message);
            success.css('display','');
            form.trigger("reset");
            success.delay(800).fadeOut(300, function(){
                success.css('display','none');
            });
        }else{
            var error = modal.find('.error-modal');
            var message = '';
            $.each(data.responseJSON.errors, function (key, value) {
                message += value
            });
            console.log(message);
            error.fadeIn();
            error.html(message);
            error.css('display','');
            error.delay(800).fadeOut(300, function(){
                error.css('display','none');
            })
        }
    }
</script>
