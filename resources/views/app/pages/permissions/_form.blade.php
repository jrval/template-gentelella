<form id="form" class="form-horizontal form-label-left" >
    @csrf
    @isset($permission)
        {{ method_field('PUT') }}
    @endisset
    <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Permission Name <span class="required">*</span>
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" id="name" required="required"  name="name" value="{{$permission->name ?? ''}}" class="form-control col-md-7 col-xs-12">
        </div>
    </div>
</form>
