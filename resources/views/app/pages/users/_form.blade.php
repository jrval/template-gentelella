<form id="form" class="form-horizontal form-label-left">
    @csrf
    @isset($user)
        {{ method_field('PUT') }}
    @endisset
    <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Full Name <span class="required">*</span>
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" name="name" required="required" value="{{$user->name ?? ''}}" class="form-control col-md-7 col-xs-12">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="username">Username <span class="required">*</span>
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" name="username" required="required" value="{{$user->username ?? ''}}" class="form-control col-md-7 col-xs-12">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" name="email" required="required" value="{{$user->email ?? ''}}" class="form-control col-md-7 col-xs-12">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Password <span class="required">*</span>
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="password" name="password" required="required" value="" class="form-control col-md-7 col-xs-12">
            @isset($user)
            <small class="text-navy">Leave blank if you don't want to change password</small>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password_confirmation">Confirm Password <span class="required">*</span>
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="password" name="password_confirmation" required="required" value="" class="form-control col-md-7 col-xs-12">
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 col-sm-3 col-xs-12 control-label">Select Role <span class="required">*</span>
            <br>
            <small class="text-navy">User can have 1 or more roles</small>
        </label>
        <div class="col-md-9 col-sm-9 col-xs-12">
            @forelse($roles as $key=>$role)
                <div class="checkbox">
                    <label>
                        @if(isset($user))
                            <input type="checkbox" class="flat" name="roles[]" value="{{$role->name}}"
                                   @foreach($user->roles()->pluck('name') as $user_role)
                                   @if($user_role== $role->name)
                                   checked="checked"
                                @endif
                                @endforeach
                            > {{$role->name}}
                        @else
                            <input type="checkbox" class="flat" name="roles[]" value="{{$role->name}}" > {{$role->name}}
                        @endif
                    </label>
                </div>
            @empty
                <div class="checkbox">
                    <label>
                        No Roles
                    </label>
                </div>
            @endforelse
        </div>
    </div>
</form>
