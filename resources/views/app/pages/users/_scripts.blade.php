<script>
    var btn;
    var modal;
    var form;
    var table;
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

       table = $('#users-datatable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,

            ajax:{
                "url": '{!! route("users.datatable") !!}',
                "type": "GET",
            },
            columns: [
                { data: 'name', name: 'name' },
                { data: 'username', name: 'username' },
                { data: 'email', name: 'email'},
                { data: 'roles', name: 'roles'},
                { data: 'created_at', name: 'created_at'},
                { data: 'action', name: 'action',searchable:false,orderable:false},

            ],
           columnDefs:[
               {targets:5,"width":"13%"}
           ],
           "createdRow": function( row, data, dataIndex){

               if( data['deleted_at'] != null){
                   $(row).addClass('alert-danger');
               }
           },
            "order": [[ 0, "asc" ]], //or asc
        });

    });

    /*render create-edit modal*/
    $('#modal-lg').on('show.bs.modal', function (event) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var button = $(event.relatedTarget); // Button that triggered the modal
        btn = button.data('btn'); // Extract info from data-* attributes
        modal = $(this);
        modal.find('.btn-modal-submit').css('display','');
        form = modal.find(".modal-body #form");
        form.trigger('reset');
        if (btn === 'create') {
            var url = '{{ route("users.create")}}';
            $.ajax({
                url: url, //this is your uri
                type: 'get', //this is your method
                dataType: 'json',
                success: function (response) {
                    //console.log(response.form);
                    modal.find('.modal-title').html('CREATE USER');
                    modal.find('.modal-body').html(response.form);
                    form = modal.find(".modal-body #form");
                    $('input.flat').iCheck({
                        checkboxClass: 'icheckbox_flat-green',
                        radioClass: 'iradio_flat-green'
                    });
                    form.attr('action', '{{ route("users.store")}}');
                    form.attr('method', 'POST');
                    modal.find('.btn-modal-submit').html('Save');
                }
            })//end ajax
        } else if (btn === 'edit') {
            var user_id = button.data('id');
            var url = '{{ route("users.edit",":user_id")}}';
            url = url.replace(':user_id', user_id);
            $.ajax({
                url: url, //this is your uri
                type: 'get', //this is your method
                dataType: 'json',
                success: function (response) {
                    //console.log(response.form);
                    var url = '{{ route("users.update",":user_id")}}';
                    url = url.replace(':user_id', user_id);
                    modal.find('.modal-title').html('EDIT USER');
                    modal.find('.modal-body').html(response.form);
                    form = modal.find(".modal-body #form");
                    form.find('input.flat').iCheck({
                        checkboxClass: 'icheckbox_flat-green',
                        radioClass: 'iradio_flat-green'
                    });
                    form.attr('action', url);
                    form.attr('method', 'POST');
                    modal.find('.btn-modal-submit').html('Update');
                }
            })//end ajax
        }
    });

    $('#modal-lg-delete').on('show.bs.modal', function (event) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var button = $(event.relatedTarget); // Button that triggered the modal
        var user_id = button.data('id');// Extract info from data-* attributes

        modal = $(this);
        modal.find('.btn-modal-submit').css('display','');
        btn = button.data('btn');
        if(btn==='delete'){
            var url = '{{ route("users.destroy", ":user_id") }}';
            url = url.replace(':user_id', user_id);
            form = modal.find('.modal-body #form-delete');
            form.attr('action', url);
            modal.find('.modal-title').html('DISABLED USER');
            modal.find('.modal-body #message').html(' <strong>User will be DISABLED!</strong> Are you sure to process this transaction?');
            modal.find('.btn-modal-submit').html('Disabled');
        }else if(btn==='enable'){
            var url = '{{ route("users.enabled", ":user_id") }}';
            url = url.replace(':user_id', user_id);
            form = modal.find('.modal-body #form-delete');
            form.attr('action', url);
            modal.find('.modal-title').html('ENABLED USER');
            modal.find('.modal-body #message').html(' <strong>User will be ENABLED!</strong> Are you sure to process this transaction?');
            modal.find('.btn-modal-submit').html('Enable');
        }

    });

    /*AJAX SUBMIT*/
    $(".btn-modal-submit").click(function (e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        var url =  form.attr('action');
        console.log(url);
        $.ajax({
            type: form.attr('method'),
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function (data) {
                checkValidation(form,data);
                $('#users-datatable').DataTable().ajax.reload();
                if(btn !== 'delete' || btn !=='enable'){
                    modal.find('.modal-body #name').val(data.name)
                }
            }, error: function (xhr, ajaxOptions, thrownError) {
                checkValidation(form,xhr)
            }
        });
    });

    /*functions*/

    function checkValidation(form,data){
        if(data.status==='success'){
            var success = modal.find('.success-modal');
            success.fadeIn();
            success.html(data.message);
            success.css('display','');
            form.trigger("reset");
            if(btn==='create'){
                form.find('input.flat').iCheck('update');
            }

            if(btn==='delete' || btn ==='enable'){
                modal.find('.btn-modal-submit').css('display','none');
            }
            success.delay(1000).fadeOut(300, function(){
                success.css('display','none');
                if(btn==='delete' || btn ==='enable'){
                    modal.modal('hide');
                }
            });
        }else{
            var error = modal.find('.error-modal');
            var message = '';
            var count = 0;
            $.each(data.responseJSON.errors, function (key, value) {
                count++;
                if(count>1){
                    message+='<br>';
                }
                message += value
            });
            console.log(message);
            error.fadeIn();
            error.html(message);
            error.css('display','');
            error.delay(1000).fadeOut(300, function(){
                error.css('display','none');
            })
        }
    }

</script>
